# Google Proxy

In the CS50's Training, I had to code a google proxy from scratch using HTML and CSS.
We gonna use this code to launch this website in the cloud and make it highly available thanks to K8S.
To do so we'll use the devops techniques and automate the deployment by creating a CI/CD pipeline. 

Sources : 
https://medium.com/faun/application-deployment-using-gitlab-ci-cd-on-managed-kubernetes-cluster-at-gcp-72b59496979c  
https://docs.gitlab.com/runner/executors/kubernetes.html  
https://docs.gitlab.com/ee/ci/docker/using_kaniko.html   
https://gist.github.com/PurpleBooth/109311bb0361f32d87a2  





# Google Proxy -- Gitops deployment --

The aim of this project is to launch an application in the cloud using the Gitlab devops tools.  

In the CS50's Training, I had to code a google proxy from scratch using HTML and CSS.
We gonna use that code to launch this website in the cloud and make it highly available thanks to K8S.
To do so we'll use the devops techniques and automate the deployment by creating a CI/CD pipeline.

## Getting Started

The idea here is to automate the deployment of an application on a K8S cluster.  
The code of the application is hosted on Gitlab.  
When a version tag will be create, a CI/CD pipeline will be triggered to deploy the code on K8S.  

The pipeline won't be too complicated, we only deploy a webserver here.   
It will look like this:  * build * deploy * test

Kaniko will be used to build docker images.

### Prerequisites

- Gitlab
- K8S cluster 
- [Gitlab K8S integration](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html) - Dependency Management 


### Organizing your repo

I decided to put all the website html/CSS files into the "www" folder as the root path is mainly for the "devops" files.  

```
.
├── Dockerfile            }
├──.gitlab-ci.yaml        | "Devops files"
├── kube-deployment.yaml  }
├── README.md              
└── www
    ├── advancedSearch.html
    ├── advancedSearchStyle.css
    ├── google_adv.html
    ├── images
    ├── imagesSearch.html
    ├── index.html
    ├── logoGimages.css
    ├── pageStyle.css
    └── searchBarSyle.css

```


## CI/CD pipeline (.gitlab-ci.yaml)
 
The CI/CD pipeline of the project is defined in the `.gitlab-ci.yaml` file.

### Build stage

Here we declare to the runner to deploy a kaniko pod in the cluster and build the project image.  

```
build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG

```

### Deploy stage

Here we declare to our runner to deploy our application in the cluster.  
To do so, we'll set a sed function to parse the templated values.

```
deploy_live:
  image:
    name: lachlanevenson/k8s-kubectl:latest
    entrypoint: ["/bin/sh", "-c"]
  stage: deploy
  environment:
    name: live
    url: $AUTO_DEVOPS_DOMAIN
  only:
    - tags
  when: manual
  script:
    - kubectl version
    - sed_files deployment
    - kubectl apply -f kube-deployment.yml
    - kubectl get deploy,svc,ing,pod -l app="$(echo ${CI_PROJECT_NAME} | tr "." "-")",ref="${CI_ENVIRONMENT_SLUG}"

```
We can see in the scripts section a sed_files function.  
To be able to deploy several version of this app, we use a templating function which uses the power of Gitlab variables.
Here is the embedded function declaration:  

``` 
.helper_functions: &helper_functions_template |
  function sed_files() {
    file_type=${1:-compose}
    
    echo "File Type is $file_type"

    if [[ "$file_type" == "deployment" ]]
    then 
      echo "Applying SED on Deployment Yamls"
      sed -i "s~__CI_REGISTRY_IMAGE__~${CI_REGISTRY_IMAGE}~" kube-deployment.yml
      sed -i "s/__CI_ENVIRONMENT_SLUG__/${CI_ENVIRONMENT_SLUG}/" kube-deployment.yml
      sed -i "s/__VERSION__/${CI_COMMIT_REF_NAME}/" kube-deployment.yml
      sed -i "s/__CI_PROJECT_NAME__/${CI_PROJECT_NAME}/" kube-deployment.yml
      sed -i "s/__CI_PROJECT_PATH_SLUG__/${CI_PROJECT_PATH_SLUG}/" kube-deployment.yml
    else
      echo "Applying SED on compose Yamls"
      sed -i "s~__CI_REGISTRY_IMAGE__~${CI_REGISTRY_IMAGE}~" docker-compose.yml
    fi
  }


before_script:
  - *helper_functions_template

```

### Test stage



## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Kaniko](https://github.com/GoogleContainerTools/kaniko) - The Google container image builder
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
